

# devices[0]
# {0: 'Microsoft Sound Mapper - Input'}
# devices[1]
# {1: 'Microphone (HD Pro Webcam C920)'}
# devices[2]
# {2: 'S/PDIF Input (Komplete Audio 6 '}
# devices[3]
# {3: 'Input 1/2 (Komplete Audio 6 WDM'}
# devices[4]
# {4: 'Input 3/4 (Komplete Audio 6 WDM'}

default_input_device_index = 3
default_output_device_index = 1