import pyaudio
import pyAudioAnalysis
import wave
import time

import config

audio = pyaudio.PyAudio()
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024

def play_file(filename, device=config.default_output_device_index):

    wf = wave.open(filename, 'rb')

    # define callback (2)
    def callback(in_data, frame_count, time_info, status):
        data = wf.readframes(frame_count)
        return (data, pyaudio.paContinue)

    # open stream using callback (3)
    stream = audio.open(format=audio.get_format_from_width(audio.get_sample_size(FORMAT)),
                        channels=CHANNELS,
                        rate=RATE,
                        output=True,
                        #output_device_index=device,
                        stream_callback=callback)

    print("playing file {}".format(filename))
    # start the stream (4)
    stream.start_stream()

    # wait for stream to finish (5)
    while stream.is_active():
        time.sleep(0.1)

    # stop stream (6)
    stream.stop_stream()
    stream.close()
    wf.close()

    print("finished playing")

def start_recording(length=5, filename="file.wav", device=config.default_input_device_index):
    RECORD_SECONDS = length
    WAVE_OUTPUT_FILENAME = filename

    # start Recording
    stream = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True,
                        frames_per_buffer=CHUNK,
                        input_device_index=device)
    print("recording...")
    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)
    print("finished recording")

    # stop Recording
    stream.stop_stream()
    stream.close()
    audio.terminate()

    waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    # TODO: the below info did not get saved! Fix that
    waveFile.setnchannels(CHANNELS)
    waveFile.setsampwidth(audio.get_sample_size(FORMAT))
    waveFile.setframerate(RATE)
    waveFile.writeframes(b''.join(frames))
    waveFile.close()

def list_input_interface():
    info = audio.get_host_api_info_by_index(0)
    numdevices = info.get('deviceCount')
    devices = []
    for i in range(0, numdevices):
        if (audio.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
            devices.append((i, audio.get_device_info_by_host_api_device_index(0, i).get('name')))
    return devices

def list_output_interfaces():
    info = audio.get_host_api_info_by_index(0)
    numdevices = info.get('deviceCount')
    devices = []
    for i in range(0, numdevices):
        if (audio.get_device_info_by_host_api_device_index(0, i).get('maxOutputChannels')) > 0:
            devices.append((i, audio.get_device_info_by_host_api_device_index(0, i).get('name')))
    return devices