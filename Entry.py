import sys
import PySide2.QtCore
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QFile

import audio
import config


def record():
    audio.start_recording(length=window.length_spinbox.value(), filename=window.filename_lineedit.text(), device=config.input_device_index)

def play():
    audio.play_file(filename=window.filename_lineedit.text(), device=config.input_device_index)

def set_input_device(index):
    print("Input index: {}".format(index))
    config.input_device_index = index

def set_output_device(index):
    print("Output index: {}".format(index))
    config.output_device_index = index

def setup_ui(window):
    # Connect buttons
    window.record_button.clicked.connect(record)
    window.play_button.clicked.connect(play)

    # Populate device comboboxes
    input_devices = audio.list_input_interface()
    output_devices = audio.list_output_interfaces()
    for device in input_devices:
        window.input_device_combobox.insertItem(device[0], device[1])
    for device in output_devices:
        window.output_device_combobox.insertItem(device[0], device[1])

    # Connect combobox signals
    window.input_device_combobox.currentIndexChanged.connect(set_input_device)
    window.output_device_combobox.currentIndexChanged.connect(set_output_device)

    # Set current index
    window.input_device_combobox.setCurrentIndex(config.default_input_device_index)
    window.output_device_combobox.setCurrentIndex(config.default_output_device_index)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    ui_file = QFile("mainwindow.ui")
    ui_file.open(QFile.ReadOnly)

    loader = QUiLoader()
    window = loader.load(ui_file)
    setup_ui(window)

    ui_file.close()
    window.show()

    sys.exit(app.exec_())
